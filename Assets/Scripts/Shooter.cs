﻿using SleepingTiger.Core;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;


namespace SleepingTiger
{
    public class Shooter : MonoBehaviour
    {
        [Tooltip("Event trigger from shoot button")]
        [SerializeField] private EventTrigger _eventTrigger;
        [Tooltip("Strength indicator")]
        [SerializeField] private Slider _slider;

        [SerializeField] private BirdDestroyer _birdDestroyer;

        [Space] 
        [Header("Ability settings")] 
        [SerializeField] private float _doubleScoreActivityTime;
        [SerializeField] private float _shootWithoutStrengthActivityTime;
        
        [Space] 
        [Header("Ability buttons")] 
        [SerializeField] private Button _doubleScoreAbilityButton;
        [SerializeField] private Button _shootWithoutStrengthAbiltyButton;

        [Space] 
        [Header("Ability count text")] 
        [SerializeField] private TMP_Text _doubleShootAbilityText;
        [SerializeField] private TMP_Text _shootWithoutStrengthAbiltyText;
        

        private EventTrigger.Entry _onPointerDownEntry;
        private EventTrigger.Entry _onPointerUpEntry;

        private float _pressTime;
        private bool _isPressed;
        private AbilityActivator _abilityActivator;

        private void Awake()
        {
            _abilityActivator = new AbilityActivator(_doubleScoreActivityTime, 
                _shootWithoutStrengthActivityTime, _doubleShootAbilityText, _shootWithoutStrengthAbiltyText);
            _onPointerDownEntry = new EventTrigger.Entry { eventID = EventTriggerType.PointerDown };
            _onPointerDownEntry.callback.AddListener(OnPointerDown);
            
            _onPointerUpEntry = new EventTrigger.Entry { eventID = EventTriggerType.PointerUp };
            _onPointerUpEntry.callback.AddListener(OnPointerUp);
        }

        private void OnEnable()
        {
            _doubleScoreAbilityButton.onClick.AddListener(_abilityActivator.ActivateDoubleScore);
            _shootWithoutStrengthAbiltyButton.onClick.AddListener(_abilityActivator.ActivateShootWithoutStrength);
            
            _eventTrigger.triggers.Add(_onPointerDownEntry);
            _eventTrigger.triggers.Add(_onPointerUpEntry);
        }

        private void OnDisable()
        {
            _doubleScoreAbilityButton.onClick.RemoveListener(_abilityActivator.ActivateDoubleScore);
            _shootWithoutStrengthAbiltyButton.onClick.RemoveListener(_abilityActivator.ActivateShootWithoutStrength);
            
            _eventTrigger.triggers.Remove(_onPointerDownEntry);
            _eventTrigger.triggers.Remove(_onPointerUpEntry);
        }

        private void Update()
        {
            UpdateSlider();
            _abilityActivator.Update();
        }

        private void UpdateSlider()
        {
            if (_isPressed)
            {
                if(_pressTime <= _slider.maxValue)
                    _pressTime += Time.deltaTime;
            }
            else
            {
                if (_pressTime >= 0.0f)
                    _pressTime -= Time.deltaTime;
            }
            _slider.value = _pressTime;
        }

        private void OnPointerDown(BaseEventData eventData) => _isPressed = true;

        private void OnPointerUp(BaseEventData eventData)
        {
            _isPressed = false;
            _birdDestroyer.Shoot(_pressTime, _abilityActivator.DoubleScoreIsActive, _abilityActivator.ShotWithoutStrengthIsActive);
        }
    }
}