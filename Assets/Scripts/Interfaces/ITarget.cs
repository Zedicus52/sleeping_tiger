﻿using System;
using UnityEngine;

namespace SleepingTiger.Interfaces
{
    public interface ITarget
    {
        event Action<ITarget> TargetAwake; 
        Vector3 GetPosition();
        void TakeDamage(float damage);
    }
}