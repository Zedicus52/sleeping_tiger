﻿using System.IO;
using UnityEngine;

namespace SleepingTiger.Utilities
{
    public class GradientGenerator : MonoBehaviour
    {
        [SerializeField] private Gradient _gradient;
        [SerializeField] private string _savingPath = "/GenerateTexture";

        [SerializeField] private float _width = 256f;
        [SerializeField] private float _height = 64f;

        private Texture2D _gradientTexture;
        private Texture2D _tempTexture;

        private Texture2D GenerateGradientTexture(Gradient gradient)
        {
            if (_tempTexture == null)
                _tempTexture = new Texture2D((int)_width, (int)_height);

            for (int x = 0; x < _width; ++x)
            {
                for (int y = 0; y < _height; ++y)
                {
                    Color color = gradient.Evaluate(0 + (x / _width));
                    _tempTexture.SetPixel(x,y,color);
                }
            }

            _tempTexture.wrapMode = TextureWrapMode.Clamp;
            _tempTexture.Apply();
            return _tempTexture;
        }

        public void BakeGradientTexture()
        {
            _gradientTexture = GenerateGradientTexture(_gradient);
            byte[] bytes = _gradientTexture.EncodeToPNG();
            string fileName = "Gradient_" + Random.Range(0, 999999) + ".png";
            string path = Path.Combine(Application.dataPath + _savingPath);
            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);
            File.WriteAllBytes(Path.Combine(path, fileName), bytes);
        }
    }
}