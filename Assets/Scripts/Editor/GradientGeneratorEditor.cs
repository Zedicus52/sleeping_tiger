﻿using SleepingTiger.Utilities;
using UnityEditor;
using UnityEngine;

namespace SleepingTiger.Editor
{
    [CustomEditor(typeof(GradientGenerator))]
    public class GradientGeneratorEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            GradientGenerator generator = (GradientGenerator)target;

            if (GUILayout.Button("Generate PNG file"))
            {
                generator.BakeGradientTexture();
                AssetDatabase.Refresh();
            }
        }
    }
}