﻿using TMPro;
using UnityEngine;

namespace SleepingTiger.UI
{
    public class LoseScreen : MonoBehaviour
    {
        [SerializeField] private TMP_Text _earnText;

        public void SetEarnText(string text) => _earnText.text = text;
    }
}