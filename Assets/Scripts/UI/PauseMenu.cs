﻿using UnityEngine;

namespace SleepingTiger.UI
{
    public class PauseMenu : MonoBehaviour
    {
        [SerializeField] private GameObject _pauseMenu;

        private float _timeScale;
        public void EnablePause()
        {
            _timeScale = Time.timeScale;
            Time.timeScale = 0.0f;
            _pauseMenu.SetActive(true);
        }

        public void DisablePause()
        {
            Time.timeScale = _timeScale;
            _pauseMenu.SetActive(false);
        }
        
    }
}