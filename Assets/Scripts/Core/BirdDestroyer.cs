﻿using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SleepingTiger.Core
{
    [RequireComponent(typeof(BirdSpawner))]
    public class BirdDestroyer : MonoBehaviour
    {
        [Space] 
        [Header("Strength settings")] 
        [Range(0.1f, 0.4f)]
        [SerializeField] private float _minAverageStrength = 0.3f;
        [Range(0.5f, 0.9f)]
        [SerializeField] private float _maxAverageStrength = 0.6f;
        
        private BirdSpawner _spawner;

        private void Awake()
        {
            _spawner = GetComponent<BirdSpawner>();
        }

        public void Shoot(float strength,bool doubleScore = false, bool withoutStrength = false)
        {
            var birds = _spawner.ActiveBirds;
            if (birds.Count <= 0) return;
            Bird birdToReturn = null;

            if (withoutStrength)
                birdToReturn = birds.OrderByDescending(b => b.DistanceToTarget).First();
            else
            {
                if (strength <= _minAverageStrength)
                {
                    birdToReturn = birds.FirstOrDefault(b => b.DistanceToTarget >= b.MaximumDistance);
                }
                else if (strength >= _minAverageStrength && strength <= _maxAverageStrength)
                {
                    birdToReturn = birds.FirstOrDefault(b => b.DistanceToTarget <= b.AverageDistance);
                }
                else if (strength >= _maxAverageStrength)
                {
                    birdToReturn = birds.FirstOrDefault(b => b.DistanceToTarget <= b.MinimumDistance);
                }
            }
            
            
            if(birdToReturn == null)
                return;
            
            if (doubleScore)
                _spawner.ReturnBirdToPool(birdToReturn, birdToReturn.BirdCost * 2);
            else
                _spawner.ReturnBirdToPool(birdToReturn, birdToReturn.BirdCost);

        }
        
        public void WeakShoot()
        {
            var birds = _spawner.ActiveBirds.Where(b => b.DistanceToTarget >= b.MaximumDistance).ToArray();
            if (birds.Length <= 0) return;
            var bird = birds[Random.Range(0, birds.Length-1)];
            _spawner.ReturnBirdToPool(bird, bird.BirdCost);
        }

        public void AverageShoot() => DisableBirdWithRange(_spawner.ActiveBirds[0].AverageDistance);
        public void StrengthShoot() => DisableBirdWithRange(_spawner.ActiveBirds[0].MinimumDistance);

        private void DisableBirdWithRange(float distance)
        {
            var birds = _spawner.ActiveBirds.Where(b => b.DistanceToTarget <= distance).ToArray();
            if (birds.Length <= 0) return;
            var bird = birds[Random.Range(0, birds.Length-1)];
            _spawner.ReturnBirdToPool(bird, bird.BirdCost);
        }
        
    }
}