﻿using SleepingTiger.Enums;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SleepingTiger.Core
{
    public class AbilityBuyer : MonoBehaviour
    {
        [SerializeField] private ScoreManager _scoreManager;
        [SerializeField] private int _price;
        [SerializeField] private Abilities _abilitie;
        [Space]
        [Header("UI")]
        [SerializeField] private Button _buyButton;
        [SerializeField] private TMP_Text _priceText;
        [SerializeField] private TMP_Text _countText;

        private void Awake()
        {
            _priceText.text = _price.ToString();
            string key = _abilitie.Equals(Abilities.DoubleScore)
                ? SleepingTigerPlayerPrefs.DoubleScoreKey
                : SleepingTigerPlayerPrefs.ShootKey;
            _countText.text = SleepingTigerPlayerPrefs.GetIntByKey(key).ToString();
        }

        private void OnEnable() => _buyButton.onClick.AddListener(OnAbilityBuy);

        private void OnDisable() => _buyButton.onClick.RemoveListener(OnAbilityBuy);

        private void OnAbilityBuy()
        {
            if (_scoreManager.Buy(_price) == false)
            {
                PopupManager.Instance.ShowNotEnoughMoney();
                return;
            }
            
            if (_abilitie.Equals(Abilities.DoubleScore))
                AddAbility(SleepingTigerPlayerPrefs.DoubleScoreKey);
            else if (_abilitie.Equals(Abilities.ShootWithoutStrength))
                AddAbility(SleepingTigerPlayerPrefs.ShootKey);
        }

        private void AddAbility(string abilityKey)
        {
            int count = SleepingTigerPlayerPrefs.GetIntByKey(abilityKey);
            count += 1;
            _countText.text = count.ToString();
            SleepingTigerPlayerPrefs.SaveInt(abilityKey, count);
        }
        
    }
}