﻿using System;
using System.Collections;
using System.Collections.Generic;
using SleepingTiger.Interfaces;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SleepingTiger.Core
{
    public class BirdSpawner : MonoBehaviour
    {
        public static event Action<int> BirdDestroyed; 
        public List<Bird> ActiveBirds => _activeBirds;
        
        [SerializeField] private Tiger _tiger;
        
        [Space]
        [Header("Pool settings")]
        [SerializeField] private Bird _birdPrefab;
        [SerializeField] private Transform _parentForBirds;
        [SerializeField] private int _basicCount = 20;
        [SerializeField] private bool _isAutoExpand = true;
        
        [Space]
        [Header("Spawn settings")]
        [SerializeField] private float _spawnCooldown = 1.3f;
        [SerializeField] private Transform[] _spawnPositions;

        private bool _canSpawn;
        private Pool<Bird> _birdPool;
        private List<Bird> _activeBirds;
        private Coroutine _spawnCoroutine;

        private void Awake()
        {
            _birdPool = new Pool<Bird>(_birdPrefab, _parentForBirds, _isAutoExpand, _basicCount);
            _activeBirds = new List<Bird>();
            //StartSpawning();
        }

        public void StartSpawning()
        {
            _canSpawn = true;
            _spawnCoroutine = StartCoroutine(SpawnCycle());
        }

        public void StopSpawning()
        {
            _canSpawn = false;
            StopCoroutine(_spawnCoroutine);
        }

        private void OnEnable()
        {
            _tiger.TargetAwake += OnTargetAwake;
        }
        
        private void OnDisable()
        {
            _tiger.TargetAwake -= OnTargetAwake;
        }

        private async void OnTargetAwake(ITarget obj)
        {
            StopSpawning();
            foreach (var bird in _activeBirds)
            {
                bird.gameObject.SetActive(false);
            }

            await _tiger.StartAnimation();
            PopupManager.Instance.ShowLoseScreen();
        }

        public void ReturnBirdToPool(Bird bird, int birdCost)
        {
            if(bird == null)
                return;
            
            _activeBirds.Remove(bird);
            bird.gameObject.SetActive(false);
            BirdDestroyed?.Invoke(birdCost);
        }

        private IEnumerator SpawnCycle()
        {
            while (_canSpawn)
            {
                yield return new WaitForSeconds(_spawnCooldown);
                Bird bird = _birdPool.GetObject();
                bird.gameObject.SetActive(true);
                Vector3 position = _spawnPositions[Random.Range(0, _spawnPositions.Length - 1)].position;
                bird.Init(_tiger, position);
                _activeBirds.Add(bird);
            }
        }
    }
}