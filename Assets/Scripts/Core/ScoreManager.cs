﻿using System;
using TMPro;
using UnityEngine;

namespace SleepingTiger.Core
{
    public class ScoreManager : MonoBehaviour
    {
        public int CurrentBalance => _currentBalance;
        
        [SerializeField] private TMP_Text _scoreText;
        [SerializeField] private bool _readFromSave;
        
        private int _currentBalance;
        

        private void Awake()
        {
            if(_readFromSave)
                _currentBalance = SleepingTigerPlayerPrefs.GetIntByKey(SleepingTigerPlayerPrefs.BalanceKey);
            _scoreText.text = _currentBalance.ToString();
        }

        private void OnEnable()
        {
            BirdSpawner.BirdDestroyed += OnBirdDestroyed;
        }

        private void OnDisable()
        {
            BirdSpawner.BirdDestroyed -= OnBirdDestroyed;
        }

        private void OnDestroy()
        {
            int balance = 0;
            if (!_readFromSave)
                balance = SleepingTigerPlayerPrefs.GetIntByKey(SleepingTigerPlayerPrefs.BalanceKey);
                
            SleepingTigerPlayerPrefs.SaveInt(SleepingTigerPlayerPrefs.BalanceKey, balance + _currentBalance);
        }

        private void OnBirdDestroyed(int birdCost)
        {
            _currentBalance += birdCost;
            _scoreText.text = _currentBalance.ToString();
        }

        public bool Buy(int price)
        {
            if (_currentBalance < price)
                return false;
            _currentBalance -= price;
            _scoreText.text = _currentBalance.ToString();
            return true;
        }
    }
}