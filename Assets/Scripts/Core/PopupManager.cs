﻿using System.Collections;
using SleepingTiger.UI;
using UnityEngine;

namespace SleepingTiger.Core
{
    public class PopupManager : MonoBehaviour
    {
        public static PopupManager Instance { get; private set; }

        [Space]
        [Header("Not enough money")]
        [SerializeField] private GameObject _notEnoughMoney;
        [SerializeField] private float _showTimeNotEnoughMoney = 0.5f;
        
        [Space]
        [Header("Not enough ability")]
        [SerializeField] private GameObject _notEnoughAbility;
        [SerializeField] private float _showTimeNotEnoughAbility = 0.8f;

        [Space] 
        [Header("Lose screen")] 
        [SerializeField] private ScoreManager _scoreManager;
        [SerializeField] private LoseScreen _loseScreen;

        

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                return;
            }
            Destroy(gameObject);
        }

        public void ShowNotEnoughMoney() => StartCoroutine(ShowPopup(_notEnoughMoney, _showTimeNotEnoughMoney));

        public void ShowNotEnoughAbility() => StartCoroutine(ShowPopup(_notEnoughAbility, _showTimeNotEnoughAbility));
        public void ShowLoseScreen()
        {
            _loseScreen.gameObject.SetActive(true);
            _loseScreen.SetEarnText($"You earn: {_scoreManager.CurrentBalance}");
        }

        private IEnumerator ShowPopup(GameObject panel, float time)
        {
            if (!panel)
                yield break;
            
            panel.SetActive(true);
            yield return new WaitForSeconds(time);
            panel.SetActive(false);
        }
    }
}