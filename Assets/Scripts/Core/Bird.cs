﻿using System;
using System.Collections;
using SleepingTiger.Interfaces;
using UnityEngine;

namespace SleepingTiger.Core
{
    public class Bird : MonoBehaviour
    {

        public float DistanceToTarget => 
            _target != null ? Vector3.Distance(_targetPosition, _transform.position) : 0.0f;

        public int BirdCost => _birdCost;

        public float MinimumDistance => _minimumDistance;
        public float AverageDistance => _averageDistance;
        public float MaximumDistance => _maximumDistance;

        [SerializeField] private float _damageCooldown;
        [SerializeField] private float _movementSpeed;
        [SerializeField] private int _birdCost = 8;
        
        [Space]
        [Header("Damage settings")]
        [SerializeField] private float _minimumDamage;
        [SerializeField] private float _averageDamage;
        [SerializeField] private float _maximumDamage;
        
        [Space]
        [Header("Distance settings")]
        [SerializeField] private float _minimumDistance;
        [SerializeField] private float _averageDistance;
        [SerializeField] private float _maximumDistance;

        private float _currentDamage;
        //[SerializeField] private Tiger _targetTransform;

        private Transform _transform;
        private ITarget _target;
        private Vector3 _targetPosition;
        private Coroutine _attackCoroutine;
        

        private void Awake()
        {
            _currentDamage = _minimumDamage;
            //_target = _targetTransform;
            _transform = GetComponent<Transform>();
            //Init(_target, _transform.position);
        }

        private void OnDisable()
        {
            if(_target == null)
                return;
            
            _target = null;
            StopCoroutine(_attackCoroutine);
        }

        private void Update()
        {
            if (_target == null)
                return;
            
            
            Vector3 transformPosition = _transform.position;

            transformPosition = Vector3.Lerp(transformPosition, _targetPosition, _movementSpeed * Time.deltaTime);
            _transform.position = transformPosition;
            float distanceToTarget = Vector3.Distance(transformPosition, _targetPosition);

            if (distanceToTarget <= _minimumDistance)
                _currentDamage = _maximumDamage;
            else if (distanceToTarget <= _averageDistance)
                _currentDamage = _averageDamage;
            else if (distanceToTarget <= _maximumDistance)
                _currentDamage = _minimumDamage;
        }

        public void Init(ITarget target, Vector3 position)
        {
            _transform.position = position;
            
            _target = target;
            _targetPosition = _target.GetPosition();
            
            Quaternion rotation = Quaternion.LookRotation(_targetPosition, Vector3.forward);
            rotation.z = rotation.x;
            rotation.x = 0;
            rotation.y = 0;
            rotation.w = 1;
            
           if (_transform.position.x <= 0)
               _transform.rotation = Quaternion.Euler(rotation.eulerAngles.x + 180f, 0, rotation.eulerAngles.z + 180f);
           else
               _transform.rotation = rotation;
           
            _currentDamage = _minimumDamage;
            _attackCoroutine = StartCoroutine(AttackCycle());
        }
        

        private IEnumerator AttackCycle()
        {
            while (_transform.gameObject.activeInHierarchy)
            {
                yield return new WaitForSeconds(_damageCooldown);
                _target.TakeDamage(_currentDamage);
                //Debug.Log($"Attack target with damage &{_currentDamage}");
            }
        }
    }
}