﻿using UnityEngine;

namespace SleepingTiger.Core
{
    public static class SleepingTigerPlayerPrefs
    {
        public static string BalanceKey { get; } = "Balance";
        public static string DoubleScoreKey { get; } = "DoubleScore";
        public static string ShootKey { get; } = "Shoot";
        
        public static int GetIntByKey(string key) => PlayerPrefs.HasKey(key) ? PlayerPrefs.GetInt(key) : 0;

        public static void SaveInt(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
            PlayerPrefs.Save();
        }

    }
}