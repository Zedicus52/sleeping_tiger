﻿using System;
using System.Threading.Tasks;
using SleepingTiger.Interfaces;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace SleepingTiger.Core
{
    public class Tiger : MonoBehaviour, ITarget
    {
        public event Action<ITarget> TargetAwake;

        [Header("UI")]
        [SerializeField] private Slider _awakeningSlider;

        [Space] 
        [Header("Awakening settings")] 
        [SerializeField] private float _maxAwakening;

        [SerializeField] private Transform[] _birdTargets;

        [SerializeField] private Sprite _roaringTiger;
        [SerializeField] private SpriteRenderer _renderer;

        private float _currentAwakening;

        private void Awake()
        {
            _currentAwakening = _maxAwakening;
            _awakeningSlider.maxValue = _maxAwakening;
            _awakeningSlider.minValue = 0.0f;
            _awakeningSlider.value = 0.0f;
        }

        public Vector3 GetPosition() => _birdTargets[Random.Range(0, _birdTargets.Length - 1)].position;

        public void TakeDamage(float damage)
        {
            _currentAwakening -= damage;
            _awakeningSlider.value = _maxAwakening - _currentAwakening;
            if (_currentAwakening <= 0)
            {
                TargetAwake?.Invoke(this);
            }
        }

        public async Task StartAnimation()
        {
            _awakeningSlider.gameObject.SetActive(false);
            _renderer.sprite = _roaringTiger;
            Vector3 localScale = _renderer.gameObject.transform.localScale;
            _renderer.gameObject.transform.localScale = new Vector3(localScale.x * -1, localScale.y, localScale.z);
            await Task.Delay(2000);
        }
    }
}