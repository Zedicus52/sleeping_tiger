﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace SleepingTiger.Core
{
    public class SceneLoader : MonoBehaviour
    {
        [SerializeField] private int _sceneBuildIndex;

        public void LoadScene()
        {
            SceneManager.LoadScene(_sceneBuildIndex);
        }
    }
}