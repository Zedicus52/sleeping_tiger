﻿using TMPro;
using UnityEngine;

namespace SleepingTiger.Core
{
    public class AbilityActivator
    {
        public bool DoubleScoreIsActive { get; private set; }
        public bool ShotWithoutStrengthIsActive { get; private set; }
        
        private readonly float _doubleScoreActivityTime;
        private readonly float _shootActivityTime;

        private readonly TMP_Text _doubleScoreText;
        private readonly TMP_Text _shootWithoutStrengthText;

        private float _doubleScoreTimeLeft;
        private float _shootTimeLeft;

        public AbilityActivator(float doubleScoreActivityTime, float shootActivityTime, TMP_Text doubleText, TMP_Text shootText)
        {
            _doubleScoreActivityTime = doubleScoreActivityTime;
            _shootActivityTime = shootActivityTime;
            
            _doubleScoreText = doubleText;
            _shootWithoutStrengthText = shootText;
            
            _doubleScoreText.text =
                SleepingTigerPlayerPrefs.GetIntByKey(SleepingTigerPlayerPrefs.DoubleScoreKey).ToString();
            _shootWithoutStrengthText.text =
                SleepingTigerPlayerPrefs.GetIntByKey(SleepingTigerPlayerPrefs.ShootKey).ToString();
            
            DoubleScoreIsActive = false;
            ShotWithoutStrengthIsActive = false;
        }

        public void ActivateDoubleScore()
        {
            if(DoubleScoreIsActive)
                return;
            
            int count = SleepingTigerPlayerPrefs.GetIntByKey(SleepingTigerPlayerPrefs.DoubleScoreKey);
            if (count <= 0)
            {
                PopupManager.Instance.ShowNotEnoughAbility();
                return;
            }
            
            SleepingTigerPlayerPrefs.SaveInt(SleepingTigerPlayerPrefs.DoubleScoreKey, count-1);
            _doubleScoreText.text = (count-1).ToString();
            
            _doubleScoreTimeLeft = _doubleScoreActivityTime;
            DoubleScoreIsActive = true;
        }

        public void ActivateShootWithoutStrength()
        {
            if(ShotWithoutStrengthIsActive)
                return;
            
            int count = SleepingTigerPlayerPrefs.GetIntByKey(SleepingTigerPlayerPrefs.ShootKey);
            if (count <= 0)
            {
                PopupManager.Instance.ShowNotEnoughAbility();
                return;
            }
            
            SleepingTigerPlayerPrefs.SaveInt(SleepingTigerPlayerPrefs.ShootKey, count-1);
            _shootWithoutStrengthText.text = (count-1).ToString();

            _shootTimeLeft = _shootActivityTime;
            ShotWithoutStrengthIsActive = true;

        }

        public void Update()
        {
            if (DoubleScoreIsActive)
            {
                _doubleScoreTimeLeft -= Time.deltaTime;
                if (_doubleScoreTimeLeft <= 0)
                    DoubleScoreIsActive = false;
            }
            
            if (ShotWithoutStrengthIsActive)
            {
                _shootTimeLeft -= Time.deltaTime;
                if (_shootTimeLeft <= 0)
                    ShotWithoutStrengthIsActive = false;
            }
        }
        
        
    }
}