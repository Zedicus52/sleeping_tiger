﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SleepingTiger.Core
{
    public class GameBootstrap : MonoBehaviour
    {
        [Header("Tutorial Settings")]
        [TextArea]
        [SerializeField] private List<string> _tutorialSteps;
        [SerializeField] private Button _mainButton;
        [SerializeField] private TMP_Text _tutorialText;


        [Space] 
        [Header("Counter settings")] 
        [SerializeField] private List<string> _counterTexts;
        [SerializeField] private TMP_Text _counterText;
        [SerializeField] private float _showTime = 0.3f;

        [Space] 
        [SerializeField] private BirdSpawner _spawner;

        private int _currentStep;

        private void Awake()
        {
            _tutorialText.text = _tutorialSteps[0];
            _currentStep = 0;
        }

        private void OnEnable()
        {
            _mainButton.onClick.AddListener(OnButtonClick);
        }

        private void OnDisable()
        {
            _mainButton.onClick.RemoveListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            if (_currentStep >= _tutorialSteps.Count - 1)
            {
                _mainButton.gameObject.SetActive(false);
                StartCoroutine(ShowCounter());
                return;
            }
            
            _tutorialText.text = _tutorialSteps[++_currentStep];
        }

        private IEnumerator ShowCounter()
        {
            foreach (var text in _counterTexts)
            {
                _counterText.text = text;
                yield return new WaitForSeconds(_showTime);
            }
            _counterText.gameObject.SetActive(false);
            _spawner.StartSpawning();
        }

        private void OnValidate()
        {
            if (_tutorialSteps == null)
            {
                Debug.LogError("Tutorial must contains one or more steps"); 
                return;
            }
            
            if(_tutorialSteps.Count <= 0)
                Debug.LogError("Tutorial must contains one or more steps");
        }
    }
}